#needsrootforbuild
%global __cargo_skip_build 0
%global __cargo_common_opts %{?__cargo_common_opts} --all
Name:           sysboost
Version:        1.0.0
Release:        2
Summary:        Sysboost Rpm Build
License:        Mulan PSL v2
URL:            https://gitee.com/openeuler/sysboost

Source0:        %{name}.tar.xz

BuildRequires: meson >= 0.49.2, clang >= 12.0.1
BuildRequires: make >= 4.0, bison >= 2.7, binutils >= 2.30-17, gcc >= 10.3.1
BuildRequires: rust rust-packaging cargo
BuildRequires: native-turbo-devel
BuildRequires: ncurses-devel
BuildRequires: xz-devel
BuildRequires: kernel-devel

%define kern_devel_ver %(ver=`rpm -qa|grep kernel-devel`;echo ${ver#*kernel-devel-})

%description
sysboost can merge ELF files to improve performance.

%package profile
Summary: Indicates the preset profile for the bolt.

%description profile
Indicates the preset profile for the bolt..

%prep
export RPM_BUILD_DIR=%_topdir/BUILD
export RPM_BUILD_SOURCE=%_topdir/SOURCES

tar xf $RPM_BUILD_SOURCE/%{name}.tar.xz

%build
# build sysboost_loader.ko
namer=%{kern_devel_ver}
pushd $RPM_BUILD_DIR/%{name}/src/sysboost_loader
%ifarch aarch64
make %{?_smp_mflags} ARCH=arm64 KDIR=/lib/modules/${namer}/build
%else
# make %{?_smp_mflags} ARCH=x86_64 KDIR=/lib/modules/${namer}/build
%endif
popd

# build sysboostd
pushd $RPM_BUILD_DIR/%{name}/src/sysboostd
%cargo_build -a
popd

# build elfmerge
pushd $RPM_BUILD_DIR/%{name}
%meson
%meson_build
popd

%install
# prep install path
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system
mkdir -p $RPM_BUILD_ROOT/lib/modules/sysboost
mkdir -p $RPM_BUILD_ROOT/etc/sysboost.d/
mkdir -p $RPM_BUILD_ROOT/var/lib/sysboost/
mkdir -p $RPM_BUILD_ROOT/usr/lib/sysboost.d/profile/
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/lib/relocation

# install binfmt.ko
%ifarch aarch64
pushd $RPM_BUILD_DIR/%{name}
install -m 400 ./src/sysboost_loader/sysboost_loader.ko $RPM_BUILD_ROOT/lib/modules/sysboost/
popd
%endif

pushd $RPM_BUILD_DIR/%{name}
install -D -p ./src/sysboost.service/%{name}.service $RPM_BUILD_ROOT/usr/lib/systemd/system/%{name}.service
install -D -p ./src/sysboost.service/%{name}d_exec_stop.sh $RPM_BUILD_ROOT/etc/systemd/system/%{name}d_exec_stop.sh
install -D -p ./src/sysboostd/target/release/sysboostd $RPM_BUILD_ROOT%{_bindir}/
xz -dk ./profile/*.xz
install -D -p ./profile/* $RPM_BUILD_ROOT/usr/lib/sysboost.d/profile/
%meson_install
popd

# install sysboost_static_template
pushd $RPM_BUILD_ROOT/usr/bin
xz ./sysboost_static_template
mv -f ./sysboost_static_template.xz ./sysboost_static_template.relocation
install -D -p ./sysboost_static_template.relocation $RPM_BUILD_ROOT/usr/lib/relocation
popd

%check

%files
%defattr(400,root,root,-)
%dir %attr(700, root, root) /etc/sysboost.d/
%dir %attr(700, root, root) /var/lib/sysboost/
%dir %attr(500, root, root) /lib/modules/sysboost/
%dir %attr(500, root, root) /usr/lib/relocation
%{_bindir}/*
%{_libdir}/*
%attr(500, root, root) /usr/bin/elfmerge
%attr(500, root, root) /usr/bin/sysboostd
%attr(600, root, root) /etc/sysboost.d
/usr/lib/systemd/system/%{name}.service
/etc/systemd/system/%{name}d_exec_stop.sh
%ifarch aarch64
%attr(0400,root,root)  /lib/modules/sysboost/*
%endif
%attr(500, root, root) /usr/lib/relocation/sysboost_static_template.relocation

%files profile
%dir %attr(500, root, root)  /usr/lib/sysboost.d/profile/
%attr(0400,root,root)  /usr/lib/sysboost.d/profile/*

%doc

%changelog
* Tue Oct 17 2023 Liu Yuntao <liuyuntao10@huawei.com> - 1.0.0-2
- DESC: 1. sync from openeuler-23.09
-       2. update pathes corresponding to code refactor

* Thu Aug 24 2023 liutie <liutie4@huawei.com> - 1.0.0-1
- ID:NA
- SUG:NA
- DESC: remove post scriptlet

* Tue Jun 13 2023 liutie <liutie4@huawei.com> - 1.0.0-0
- ID:NA
- SUG:NA
- DESC: init sysboost
